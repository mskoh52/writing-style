from .ngram import count_char_ngrams
from ..constants import datadir, dbengine
from ..log_setup import setup_logging
from ..sql_models import make_lut_model, make_counts_model, Base
from sklearn.feature_extraction import DictVectorizer
from sqlalchemy.orm import sessionmaker
import argparse
import numpy as np
import os
import pandas as pd
import pickle
import spacy
from scipy.sparse import csr_matrix
import scipy.sparse

logger = setup_logging(__name__)

# Feature ideas:

# Char level:

# sent length
# n-gram frequencies

# Word level:

# DONE avg sent length
# DONE avg word length
# DONE vocab size
# vocab set distance
# word frequency rank distance
# non-standard spelling
# DONE n-gram counts

# Grammatical features

# Passive voice
# other features of parse tree?

def spacy_parse_docs(nlp, docs: pd.DataFrame) -> pd.DataFrame:
    # TODO fix tokenization of certain characters (--, ", ') after punctuation

    def count_pos(sentence, pos):
        count = 0
        for w in sentence:
            if w.pos_ == pos:
                count += 1
        return count

    length = []
    word_count = []
    vocab_size = []
    mean_sentence_length = []
    std_sentence_length = []
    word_length = []
    mean_adj_per_sentence = []
    std_adj_per_sentence = []
    mean_noun_per_sentence = []
    std_noun_per_sentence = []
    mean_verb_per_sentence = []
    std_verb_per_sentence = []
    mean_adv_per_sentence = []
    std_adv_per_sentence = []
    for n, doc in enumerate(nlp.pipe(docs.text, batch_size=1000, n_threads=8)):
        print('{0}/{1}'.format(n+1, len(docs)), end='\r')
        length.append(len(str(doc)))
        wc = doc.count_by(spacy.attrs.ORTH)
        word_count.append(wc)
        vocab_size.append(len(wc))
        mean_sentence_length.append(np.mean([len(s) for s in doc.sents]))
        std_sentence_length.append(np.std([len(s) for s in doc.sents]))
        word_length.append(np.mean([len(w) for w in doc]))
        mean_adj_per_sentence.append(np.mean([count_pos(s, 'ADJ') for s in doc.sents]))
        std_adj_per_sentence.append(np.std([count_pos(s, 'ADJ') for s in doc.sents]))
        mean_noun_per_sentence.append(np.mean([count_pos(s, 'NOUN') for s in doc.sents]))
        std_noun_per_sentence.append(np.std([count_pos(s, 'NOUN') for s in doc.sents]))
        mean_verb_per_sentence.append(np.mean([count_pos(s, 'VERB') for s in doc.sents]))
        std_verb_per_sentence.append(np.std([count_pos(s, 'VERB') for s in doc.sents]))
        mean_adv_per_sentence.append(np.mean([count_pos(s, 'ADV') for s in doc.sents]))
        std_adv_per_sentence.append(np.std([count_pos(s, 'ADV') for s in doc.sents]))

    feature_names = ['length',
                     'word_count',
                     'vocab_size',
                     'mean_sentence_length',
                     'std_sentence_length',
                     'word_length',
                     'mean_adj_per_sentence',
                     'std_adj_per_sentence',
                     'mean_noun_per_sentence',
                     'std_noun_per_sentence',
                     'mean_verb_per_sentence',
                     'std_verb_per_sentence',
                     'mean_adv_per_sentence',
                     'std_adv_per_sentence']

    features = [length,
                word_count,
                vocab_size,
                mean_sentence_length,
                std_sentence_length,
                word_length,
                mean_adj_per_sentence,
                std_adj_per_sentence,
                mean_noun_per_sentence,
                std_noun_per_sentence,
                mean_verb_per_sentence,
                std_verb_per_sentence,
                mean_adv_per_sentence,
                std_adv_per_sentence]

    features = pd.DataFrame(dict(zip(feature_names,
                                     features)))
    features.index = docs.index
    return features, feature_names

def count_dicts_to_sparse(count_dicts):
    """Convert list of count dictionaries to a sparse matrix

    Parameters
    ----------
    count_dicts : list of dict
        List of dictionaries, keys are symbols or hashes, values are counts

    Returns
    -------
    mat : scipy.sparse.csr_matrix
        Sparse matrix of counts
    feature_names : list
        List of feature names ordered by column index
    """
    # Fit DictVectorizer
    dv = DictVectorizer(sparse=True)
    mat = dv.fit_transform(count_dicts)
    feature_names = dv.get_feature_names()
    return mat, feature_names

def store_count_dict(dbengine, table_name, indices, feature_matrix, feature_names, string_store=None):
    """Writes a count dictionary (e.g. for ngrams or words) to the database

    Parameters
    ----------
    dbengine : sqlalchemy.engine.base.Engine
    table_name : str
    indices : iterable(int)
        indices from pandas DataFrame or Series (from df.index)
    count_dicts : iterable(dict)
    string_store : spacy.strings.StringStore
        StringStore from the current nlp model (from nlp.vocab.strings)

    Outputs
    -------
    none

    Side-effects
    ------------
    database has new tables with following schemata:
        table_name_lut: col (int, primary key)
                        symbol (str)
                        [hash (int)]  # only if string_store is not None
        table_name: index (int, primary key)  # use table_name_lut.col to find asociated symbol
                    symbol_counts (bytea)
    """

    Session = sessionmaker()
    Session.configure(bind=dbengine)
    session = Session()

    Lut = make_lut_model(table_name, string_store)
    Counts = make_counts_model(table_name)
    Base.metadata.create_all(dbengine)

    # add LUT to session
    for col, item in enumerate(feature_names):
        if string_store:
            symbol = string_store[item]
            session.merge(Lut(col=col, symbol=symbol, hash=item))
        else:
            session.merge(Lut(col=col, symbol=item))

    # add counts to session
    for ind, row in zip(indices, feature_matrix):
        session.merge(Counts(index=ind, symbol_counts=pickle.dumps(row)))

    session.commit()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-path', action='store')
    parser.add_argument('-p', '--pipeline', action='store')
    args = parser.parse_args()
    try:
        input_table = args.input_table
    except:
        input_table = 'chunks'
    if args.pipeline:
        pipeline = args.pipeline.split(' ')
    else:
        pipeline = 'all'

    def get_subtable(name):
        return input_table + '_' + name

    docs = pd.read_sql_table(input_table, dbengine, index_col='index').sort_index()

    if 'spacy' in pipeline or pipeline == 'all':
        nlp = spacy.load('en')
        features, feature_names = spacy_parse_docs(nlp, docs)
        # TODO it's awkward that word_counts are handled differently than ngram counts
        mat, names = count_dicts_to_sparse(features.word_count)
        store_count_dict(dbengine, get_subtable('word_count'), features.index, mat, names, string_store=nlp.vocab.strings)
        nlp.save_to_directory(os.path.join(datadir, 'latest_spacy'))
        features.drop('word_count', axis=1).to_sql(get_subtable('features'), dbengine, if_exists='replace')

    if 'ngram' in pipeline or pipeline == 'all':
        mat, names = count_dicts_to_sparse(count_char_ngrams(docs, 1))
        store_count_dict(dbengine, get_subtable('1gram_count'), docs.index, mat, names)
        mat, names = count_dicts_to_sparse(count_char_ngrams(docs, 3))
        store_count_dict(dbengine, get_subtable('3gram_count'), docs.index, mat, names)
        mat, names = count_dicts_to_sparse(count_char_ngrams(docs, 5))
        store_count_dict(dbengine, get_subtable('5gram_count'), docs.index, mat, names)
