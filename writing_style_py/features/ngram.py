from functools import partial
import multiprocessing
import pandas as pd

class NGramCounter():
    def __init__(self, n=None, counts=None):
        if counts is None:
            self.counts = {}
        else:
            self.counts = counts
        self.n = n
        # code review: is there a better way to do this?
        self.keys = self.counts.keys
        self.values = self.counts.values
        self.items = self.counts.items
        self.__getitem__ = self.counts.__getitem__

    def __setitem__(self, ix, count):
        raise NotImplementedError('Use NGramCounter.add(str_) to increment count')

    def add(self, str_):
        if self.n is None:
            self.n = len(str_)
        if len(str_) != self.n:
            raise ValueError('Attempted to insert {}-gram into {}-gram lut'.format(len(str_), self.n))
        self.counts[str_] = self.counts.setdefault(str_, 0) + 1

    def reconcile(self, old_counts):
        for k in (set(self.counts) - old_counts):
            self.counts.pop(k)
        for k in (old_counts - set(self.counts)):
            self.counts[k] = 0

    @classmethod
    def from_text(cls, text: str, n, lower=True):
        nglut = cls(n)
        if lower:
            text = text.lower()
        for pos in range(len(text) - n):
            ng = text[pos:pos+n]
            nglut.add(ng)
        return nglut

def count_char_ngrams(docs: pd.DataFrame, n: int) -> pd.DataFrame:
    """ Count character ngrams in a corpus

    Inputs
    ------
    docs : pandas.DataFrame
        The corpus
    n : int
        n-gram length

    Returns
    -------
    ngram_counters : list of NGramCounter
        list of dict-like objects containing number of each ngram
    """
    pool = multiprocessing.Pool(8)
    try:
        ngram_counters = []
        for n, ngc in enumerate(pool.imap(partial(NGramCounter.from_text, n=n), docs.text)):
            if n % 10 == 0:
                print('Counted {}/{} ngrams'.format(n, len(docs)), end='\r')
            ngram_counters.append(ngc)
    finally:
        pool.close()
        pool.join()

    return ngram_counters
