from ..constants import dbengine, outputdir
from ..features.analysis import run_lda
from sklearn.decomposition.pca import PCA
from sklearn.metrics import roc_curve, roc_auc_score, precision_recall_curve
from sklearn.preprocessing import LabelEncoder
import argparse
import numpy as np
import os
import pandas as pd
import pickle
import scipy
import scipy.spatial.distance as spd

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class AnomalyDetector():
    def __init__(self,
                 distance=spd.euclidean,
                 threshold=None):
        self.distance = distance
        self.threshold = threshold

        self.documents = pd.read_sql_table('chunks', dbengine, index_col='index').sort_index()

        author_encoder = LabelEncoder()
        self.classes = author_encoder.fit_transform(self.documents.author)

    def analyze_examples(self, train, classes, n_inliers, plot=False):
        """Compute roc, precision-recall, accuracy for some generated examples

        Inputs
        ------
        train : numpy.matrix or scipy.sparse.csr_matrix
            training examples
        classes : np.array of int
            list of classes for each example.

        """
        metrics, is_outlier, _, _ = self.generate_examples(train, classes)

        fpr, tpr, thresholds_roc = roc_curve(is_outlier, metrics)
        auroc = roc_auc_score(is_outlier, metrics)
        prec, rec, thresholds_pr = precision_recall_curve(is_outlier, metrics)

        accs = []
        for d in metrics:
            accs.append(np.mean( (metrics >= d) == is_outlier))

        if plot:
            rocfig, rocax = plot_roc(fpr, tpr)
            prfig, prax = plot_pr(prec, rec)
        else:
            rocfig, rocax = (None, None)
            prfig, prax = (None, None)

        return {'roc': (fpr, tpr, thresholds_roc, auroc, rocfig),
                'pr': (prec, rec, thresholds_pr, prfig),
                'data': (metrics, is_outlier, accs)}

    def train(self, train, classes, n_inliers, plot=True):
        """Fit training data. Training data are generated by selecting `n_inliers`
        from one class and a single outlier from a different class or the same class

        Inputs
        ------
        train : numpy.matrix or scipy.sparse.csr_matrix
            training examples
        classes : np.array of int
            list of classes for each example.

        """
        results = self.analyze_examples(train, classes, n_inliers, plot)
        metrics = results['data'][0]
        accs = results['data'][2]

        # Set threshold to give max accuracy
        self.threshold = metrics[np.argmax(accs)]
        results['threshold'] = self.threshold
        return results

    def predict(self, metrics) -> float:
        """Predict whether a test document is an outlier, given some samples
        Inputs
        ------
        sample : numpy.matrix or scipy.sparse.csr_matrix
            (sparse) matrix of positive examples where rows are documents and
            columns are feature
        sample : numpy.matrix or scipy.sparse.csr_matrix
            1 x `n_features` matrix containing the document to test

        Returns
        -------
        prediction : float
            float in range [0,1]
        """
        if self.threshold is None:
            raise ValueError('Run self.train() first...')
        # return float(scipy.stats.mode(metrics > self.threshold).mode[0])
        return np.mean(metrics) > self.threshold

    def generate_examples(self, train, classes, test_size=10, n_examples=10000, seed=50, same_author_fraction=.5):
        in_inds = {}
        out_inds = {}
        for c in np.unique(classes):
            in_inds[c] = np.nonzero(classes == c)[0]
            out_inds[c] = np.nonzero(classes != c)[0]

        metrics = np.zeros(n_examples)
        is_outlier = np.zeros(n_examples)
        sample_inds = []
        example_inds = []
        for n in range(n_examples):
            print(n, end='\r')
            c = np.random.choice(classes)
            samples = np.random.choice(in_inds[c], test_size, replace=False)
            if (same_author_fraction < 1) and (same_author_fraction > 0):
                if (np.random.rand() > same_author_fraction):
                    query = np.random.choice(out_inds[c])
                    out = 1
                else:
                    query = np.random.choice(in_inds[c])
                    out = 0
            elif same_author_fraction == 1:
                query = np.random.choice(in_inds[c])
                out = 0
            else:
                query = np.random.choice(out_inds[c])
                out = 1

            metrics[n] = self.metric(train[samples], train[query])
            is_outlier[n] = out
            sample_inds.append(samples)
            example_inds.append(query)

        return metrics, is_outlier, sample_inds, example_inds

class TxtCompDist(AnomalyDetector):
    def __init__(self,
                 distance=lambda x, y: np.sqrt((x-y).T, (x-y)),
                 combine_ops={}):
        super().__init__(distance)
        self.combine_ops = combine_ops

    def metric(self, samples, test):
        """Computes vector for samples and test then returns self.distance btwn these
        values

        """
        if len(self.combine_ops) > 0:
            if not all([(ind in range(samples.shape[1])) for inds in self.combine_ops.values() for ind in inds]):
                raise ValueError('combine_ops must define an operation for each column')
            else:
                combine_ops = self.combine_ops
        else:
            combine_ops = {'sum': np.arange(samples.shape[0])}

        v_s = np.zeros(samples.shape[1])
        for op, inds in combine_ops.items():
            v_s.flat[inds] = op(samples[:,inds])

        v_s = np.matrix(np.reshape(v_s, (-1, 1)))
        v_t = np.matrix(np.reshape(test.todense(), (-1, 1)))

        return self.distance(v_s, v_t)

class MeanDist(AnomalyDetector):
    def metric(self, samples, test):
        dists = np.zeros(samples.shape[0])
        for n in range(len(dists)):
            v_s = np.matrix(np.reshape(samples[n].todense(), (-1, 1)))
            v_t = np.matrix(np.reshape(test.todense(), (-1, 1)))
            dists[n] = self.distance(v_s, v_t)
        return np.mean(dists)

class MinDist(AnomalyDetector):
    def metric(self, samples, test):
        minim = np.inf
        for s in samples:
            v_s = np.matrix(np.reshape(s.todense(), (-1, 1)))
            v_t = np.matrix(np.reshape(test.todense(), (-1, 1)))
            dist = self.distance(v_s, v_t)
            if dist < minim:
                minim = dist
        return minim

# These don't work when using small number of examples b/c cov
# is singular.
class MeanDistMahalanobis(AnomalyDetector):
    def metric(self, samples, test):
        dists = np.zeros(samples.shape[0])
        covi = np.linalg.inv(np.cov(samples.T.todense()))
        for n in range(len(dists)):
            v_s = np.matrix(np.reshape(samples[n].todense(), (-1, 1)))
            v_t = np.matrix(np.reshape(test.todense(), (-1, 1)))
            dists[n] = spd.mahalanobis(v_s, v_t, covi)
        return np.mean(dists)

class MinDistMahalanobis(AnomalyDetector):
    def metric(self, samples, test):
        minim = np.inf
        covi = np.linalg.inv(np.cov(samples.T.todense()))
        for s in samples:
            v_s = np.matrix(np.reshape(s.todense(), (-1, 1)))
            v_t = np.matrix(np.reshape(test.todense(), (-1, 1)))
            dist = spd.mahalanobis(v_s, v_t, covi)
            if dist < minim:
                minim = dist
        return minim

def plot_roc(fpr, tpr, ax=None):
    if ax is None:
        rocfig, rocax = plt.subplots(1,1)
    else:
        rocax = ax

    rocax.plot(fpr, tpr)
    rocax.set_xlim([0.0, 1.0])
    rocax.set_ylim([0.0, 1.05])
    rocax.set_xlabel('False Positive Rate')
    rocax.set_ylabel('True Positive Rate')
    return rocfig, rocax

def plot_pr(rec, prec, ax=None):
    if ax is None:
        prfig, prax = plt.subplots(1,1)
    else:
        prax = ax

    prax.plot(rec, prec)
    prax.set_xlabel('Recall')
    prax.set_ylabel('Precision')
    prax.set_ylim([0.0, 1.05])
    prax.set_xlim([0.0, 1.0])
    return prfig, prax

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--detectors', action='append')
    args = parser.parse_args()
    detectors = args.detectors
    if not detectors:
        detectors = ['MinDist']

    documents = pd.read_sql_table('chunks', dbengine, index_col='index').sort_index()

    author_encoder = LabelEncoder()
    classes = author_encoder.fit_transform(documents.author)

    features = pickle.load(open(os.path.join(outputdir, 'features', 'features_200-100-100-200.pkl'), 'rb'))

    if os.path.exists(os.path.join(outputdir, 'anomaly', 'lda_model.pkl')):
        lda = pickle.load(open(os.path.join(outputdir, 'anomaly', 'lda_model.pkl'), 'rb'))
    else:
        os.makedirs(os.path.join(outputdir, 'anomaly'), exist_ok=True)
        lda, _, _, _ = run_lda(features, classes)
        pickle.dump(lda, open(os.path.join(outputdir, 'anomaly', 'lda_model.pkl'), 'wb'))

    features_t = scipy.sparse.csr_matrix(lda.transform(features.todense()))
    features_p = scipy.sparse.csr_matrix(PCA(n_components=9).fit_transform(features.todense()))

    colnames = list(pickle.load(open(os.path.join(outputdir, 'features', 'colnames_200-100-100-200.pkl'), 'rb')).items())
    colnames = [x[1] for x in sorted(colnames, key=lambda x: x[0])]

    results = {}
    metrics = [spd.cosine, spd.euclidean, spd.cityblock]
    for m in metrics:

        if 'TxtCompDist' in detectors:
            mean_cols = [n for n,c in enumerate(colnames) if c.find('mean') >= 0 or c.find('word_length') >= 0]
            sd_cols = [n for n,c in enumerate(colnames) if c.find('std') >= 0]
            sum_cols = [n for n,c in enumerate(colnames) if c.find('vocab_size')] + list(range(12,len(colnames)))

            combine_ops = {lambda x: np.mean(x, axis=0): mean_cols,
                           lambda x: np.sqrt(np.diag(((x*x.T)/x.shape[0]).todense())): sd_cols,  # this is wrong, but maybe it's fine
                           lambda x: np.sum(x, axis=0): sum_cols}

            tcd = TxtCompDist(distance = lambda x,y: np.sum(np.abs(x-y)),
                              combine_ops=combine_ops)
            tcd_train_result = tcd.train(features, classes, 10)
            tcd_result = tcd.analyze_examples(features, classes, 10, plot=False)
            tcd_result['threshold'] = tcd_train_result['threshold']
            results['tcd_{}'.format(m.__name__)] = tcd_result

        if 'MeanDist' in detectors:
            mean = MeanDist(distance = m)
            mean_train_result = mean.train(features, classes, 10)
            mean_result = mean.analyze_examples(features, classes, 10, plot=False)
            mean_result['threshold'] = mean_train_result['threshold']
            results['mean_{}'.format(m.__name__)] = mean_result

            mean = MeanDist(distance = m)
            mean_train_result = mean.train(features_t, classes, 10)
            mean_result = mean.analyze_examples(features_t, classes, 10, plot=False)
            mean_result['threshold'] = mean_train_result['threshold']
            results['mean_{}_lda'.format(m.__name__)] = mean_result

            mean = MeanDist(distance = m)
            mean_train_result = mean.train(features_p, classes, 10)
            mean_result = mean.analyze_examples(features_p, classes, 10, plot=False)
            mean_result['threshold'] = mean_train_result['threshold']
            results['mean_{}_pca'.format(m.__name__)] = mean_result

        if 'MinDist' in detectors:
            mini = MinDist(distance = m)
            mini_train_result = mini.train(features, classes, 10)
            mini_result = mini.analyze_examples(features, classes, 10, plot=False)
            mini_result['threshold'] = mini_train_result['threshold']
            results['mini_{}'.format(m.__name__)] = mini_result

            mini = MinDist(distance = m)
            mini_train_result = mini.train(features_t, classes, 10)
            mini_result = mini.analyze_examples(features_t, classes, 10, plot=False)
            mini_result['threshold'] = mini_train_result['threshold']
            results['mini_{}_lda'.format(m.__name__)] = mini_result

            mini = MinDist(distance = m)
            mini_train_result = mini.train(features_p, classes, 10)
            mini_result = mini.analyze_examples(features_p, classes, 10, plot=False)
            mini_result['threshold'] = mini_train_result['threshold']
            results['mini_{}_pca'.format(m.__name__)] = mini_result

    pickle.dump(results['mean_cosine_lda']['data'][0:2], open(os.path.join(outputdir, 'anomaly', 'meandist_cos_samples.pkl'), 'wb'))
    pickle.dump(results['mean_cosine_lda']['threshold'], open(os.path.join(outputdir, 'anomaly', 'meandist_threshold_cos.pkl'), 'wb'))
    pickle.dump(results, open(os.path.join(outputdir, 'anomaly', 'anomaly_results.pkl'), 'wb'))
