# Generate list of top author documents

# TODO: there are fewer unique urls than books. why?
# TODO: use database instead of pd pickle?
# TODO: force filename into {id}.txt

from ..constants import datadir
import os
import numpy as np
import pandas as pd
import re

if __name__ == '__main__':
    """
    info.groupby('author').count().iloc[:,0].sort_values(ascending=False)
    Various                                2774
    Anonymous                               694
    Shakespeare, William                    245
    Lytton, Edward Bulwer Lytton, Baron     216
    Twain, Mark                             198
    Dickens, Charles                        142
    Unknown                                 136
    Parker, Gilbert                         133
    Verne, Jules                            131
    Doyle, Arthur Conan                     121
    Dumas, Alexandre                        116
    Kingston, William Henry Giles           110
    Meredith, George                        107
    Howells, William Dean                   106
    Fenn, George Manville                   104
    Lang, Andrew                            101
    Motley, John Lothrop                    100
    Ballantyne, R. M. (Robert Michael)       97


    In [165]: set([re.match('.*/[0-9]+(.*)\.txt(\.*.*)', x).groups() if re.match('.*/[0-9]+(.*)\.txt(\.*.*)', x) else x for x in all_urls]  )
    Out[165]:
    {('-0', ''),
     'http://www.gutenberg.org/dirs/etext98/mspcd10.txt',
     ('', ''),
     'http://www.gutenberg.org/dirs/etext96/batlf10.txt',
     'http://www.gutenberg.org/dirs/etext98/allyr10.txt',
     ('', '.utf-8'),
     ('/readme-license', ''),
     ('_readme', ''),
     ('-readme', ''),
     ('-8', '')}

    filename formats: ###.txt, ###-0.txt, ###.txt.utf-8, ###-8.txt

    """

    info = pd.read_pickle(os.path.join(datadir, 'gutenberg_info.pkl'))

    not_real_authors = {'Various', 'Anonymous', 'Unknown'}
    author_counts = info.groupby('author').count().iloc[:,0].sort_values(ascending=False)
    real_authors = author_counts.reset_index().apply(lambda x: x.author not in not_real_authors, axis=1)
    author_counts = author_counts.iloc[np.argwhere(real_authors).flatten()]
    top_authors = author_counts.iloc[:10].index

    top_author_docs = info.loc[info.apply(lambda x: x.author in top_authors, axis=1)].copy()

    selected_urls = []
    for n, urls in enumerate(top_author_docs['urls']):
        for url in urls:
            if re.match('.*/[0-9]+.txt.utf-8', url):
                selected_url = url
                break
            if re.match('.*/[0-9]+-0.txt', url):
                selected_url = url
                break
            if re.match('.*[0-9]+-8.txt', url):
                selected_url = url
                break
            if re.match('.*[0-9].txt', url):
                selected_url = url
                break
        selected_urls.append(selected_url)
    top_author_docs['selected_url'] = selected_urls
    with open('top_author_urls.txt', 'w') as f:
        f.writelines([x + '\n' for x in top_author_docs['selected_url']])
