from psycopg2.extensions import register_adapter, AsIs
from sqlalchemy import Column, Integer, String, LargeBinary
from sqlalchemy.ext.declarative import declarative_base
import numpy as np

# Setup sqlalchemy stuff
Base = declarative_base()

def adapt_numpy_int64(dtype):
    return AsIs(dtype)
register_adapter(np.int64, adapt_numpy_int64)

# Define LUT
def make_lut_model(table_name, string_store=None):
    class Lut(Base):
        __tablename__ = table_name + '_lut'
        col = Column(Integer, primary_key=True)
        symbol = Column(String)
        if string_store:
            hash = Column(Integer)
    return Lut

# Define counts
def make_counts_model(table_name):
    class Counts(Base):
        __tablename__ = table_name
        index = Column(Integer, primary_key=True)
        symbol_counts = Column(LargeBinary)
    return Counts
