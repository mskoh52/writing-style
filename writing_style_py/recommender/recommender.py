from ..constants import dbengine
from ..features.analysis import FeatureAnalysis
from ..log_setup import setup_logging
from scipy.spatial.distance import cosine
import argparse
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input-path', action='store')
parser.add_argument('-p', '--pipeline', action='store')
args = parser.parse_args()
try:
    input_table = args.input_table
except:
    input_table = 'chunks'
if args.pipeline:
    pipeline = args.pipeline.split(' ')
else:
    pipeline = 'all'

logger = setup_logging(__name__)

class Recommender():
    def __init__(self):
        self.documents = pd.read_sql_table(input_table, dbengine, index_col='index').sort_index()

        analysis = FeatureAnalysis(input_table='chunks',
                                   word_n_highest=200,
                                   unigram_n_highest=100,
                                   trigram_n_highest=100,
                                   pentagram_n_highest=200,
                                   feature_file='data/features/features_200-100-100-200.pkl',
                                   colnames_file='data/features/colnames_200-100-100-200.pkl')

        self.features, self.colnames, self.docs = analysis.get_features()

    def nearest_n_docs(self, query_doc_id, n_results, ignore_doc_ids):
        """Takes document ids as input and outputs similar documents"""

        query = self.features[query_doc_id].todense()
        dists = []
        for doc in self.features:
            dists.append(cosine(query, doc.todense()))

        nearest_ids = np.argsort(dists)[::-1][1:(1+n_results)]

        return self.documents.iloc[nearest_ids]
