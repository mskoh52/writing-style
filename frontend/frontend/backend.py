# TODO maybe FeatureAnalysis needs to lose the FeatureGeneration functionality
# TODO don't use matplotlib with BytesIO for plotting, use d3 or something cool instead
from writing_style_py.constants import outputdir
from writing_style_py.classifier.anomaly import MeanDist
from writing_style_py.preprocessing.extract_text import get_sentences, get_chunks
from writing_style_py.features.analysis import FeatureAnalysis, load_old_colnames
from writing_style_py.constants import dbengine
import pickle
import scipy.spatial.distance as spd
import spacy
import queue
import threading
import pandas as pd
import numpy as np
import scipy
from io import BytesIO
import os

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class Detector():
    def __init__(self, sock):
        self.q = queue.Queue()
        self.sock = sock
        self.event = threading.Event()
        self.nlp = spacy.load('en_gutenberg')

        self.analysis = FeatureAnalysis(input_table='chunks',
                                        word_n_highest=200,
                                        unigram_n_highest=100,
                                        trigram_n_highest=100,
                                        pentagram_n_highest=200)

        self.old_luts = {}
        self.old_luts['word'] = set(pd.read_sql_query("SELECT hash FROM chunks_word_count_lut", dbengine).values.flatten())
        self.old_luts['unigram'] = set(pd.read_sql_query("SELECT symbol FROM chunks_1gram_count_lut", dbengine).values.flatten())
        self.old_luts['trigram'] = set(pd.read_sql_query("SELECT symbol FROM chunks_3gram_count_lut", dbengine).values.flatten())
        self.old_luts['pentagram'] = set(pd.read_sql_query("SELECT symbol FROM chunks_5gram_count_lut", dbengine).values.flatten())

        th = pickle.load(open(os.path.join(outputdir, 'anomaly', 'meandist_threshold_cos.pkl'), 'rb'))
        self.detector = MeanDist(distance = spd.cosine,
                                 threshold = th)
        self.detector_samples = pickle.load(open(os.path.join(outputdir, 'anomaly', 'meandist_cos_samples.pkl'), 'rb'))
        self.lda_model = pickle.load(open(os.path.join(outputdir, 'anomaly', 'lda_model.pkl'), 'rb'))
        self.colnames_dict = load_old_colnames('../data/features/colnames_200-100-100-200.pkl', 12, 200, 100, 100, 200)

    def process_items(self):
        # TODO sometimes documents don't get uploaded on spotty connection
        #      results in queue being empty, or missing items
        # TODO: make docs have default columns in case queue is empty somehow
        docs = []
        n = 0
        while True:
            try:
                text, mode = self.q.get(block=False)
                if mode == 'samples':
                    author = 'samples'
                elif mode == 'query':
                    author = 'query'
                else:
                    return 'error: invalid mode'
                docs.append(pd.Series({
                    'id': n,
                    'author': author,
                    'title': '',
                    'text': text.replace('\n', ' ')  # TODO reuse of code. very bad! if preprocessing changes, it won't be updated
                    .replace('”', '"')
                    .replace('’', "'")
                    .replace('“', '"')
                    .replace('‘', "'"),
                    'urls': '',
                }))
                n += 1
            except queue.Empty:
                break

        documents = pd.DataFrame(docs)
        chunks = get_chunks(get_sentences(documents))

        new_features, new_colnames = self.analysis.compute_new_features(chunks, self.nlp, self.old_luts, self.colnames_dict)
        new_features = scipy.sparse.csr_matrix(self.lda_model.transform(new_features.todense()))

        samples = new_features[np.nonzero(chunks.author == 'samples')[0]]

        tests = scipy.sparse.csr_matrix(new_features[np.nonzero(chunks.author == 'query')[0]])
        metrics = []
        for t in tests:
            metric = self.detector.metric(samples, t)
            metrics.append(metric)

        avg_metric = np.mean(metric)
        prediction = self.detector.predict(metrics)
        same_class_samples = self.detector_samples[0][self.detector_samples[1] == 0]
        diff_class_samples = self.detector_samples[0][self.detector_samples[1] == 1]
        fraction_greater = np.mean(same_class_samples > avg_metric)

        io = BytesIO()
        plt.hist(same_class_samples, bins=100, label='Same author')
        plt.hist(diff_class_samples, bins=100, color='r', alpha=.5, label='Different author')
        plt.axvline(x=metric, color='r', linestyle=':', label='Current document')
        plt.title('Score distribution of known examples')
        plt.legend()
        plt.savefig(io, format='svg')
        plt.close()

        #TODO will need to json.dumps this once SPA is set up
        results = {'metric': avg_metric,
                   'prediction': prediction,
                   'fraction_greater': fraction_greater,
                   'same_class_hist': io.getvalue().decode('utf-8'),
                   'samples': list(documents[documents.author == 'samples']['text']),
                   'query': documents[documents.author == 'query'].iloc[0]['text'],
                   'chunks': chunks}

        return results

    def enqueue(self, text, mode):
        self.q.put((text, mode))

class FakeDetector():
    def enqueue(self, text, mode):
        pass

    def process_items(self):
        io = BytesIO()
        plt.hist(np.random.randn(5000), bins=50)
        plt.axvline(x=.2, color='r', linestyle=':')
        plt.savefig(io, format='svg')
        plt.close()
        results = {'metric': 123.456789,
                   'prediction': 1.0,
                   'fraction_greater': .987654321,
                   'same_class_hist': io.getvalue().decode('utf-8'),
                   'samples': ["When Anu the Sublime, King of the Anunaki, and Bel, the lord of Heaven and earth, who decreed the fate of the land, assigned to Marduk, the over-ruling son of Ea, God of righteousness, dominion over earthly man, and made him great among the Igigi, they called Babylon by his illustrious name, made it great on earth, and founded an everlasting kingdom in it, whose foundations are laid so solidly as those of heaven and earth; then Anu and Bel called by name me, Hammurabi, the exalted prince, who feared God, to bring about the rule of righteousness in the land, to destroy the wicked and the evil-doers; so that the strong should not harm the weak; so that I should rule over the black-headed people like Shamash, and enlighten the land, to further the well-being of mankind.",
                               "Johannes Dei gracia rex Anglie, Dominus Hibernie, dux Normannie, Aquitannie et comes Andegravie, archiepiscopis, episcopis, abbatibus, comitibus, baronibus, justiciariis, forestariis, vicecomitibus, prepositis, ministris et omnibus ballivis et fidelibus suis salutem. Sciatis nos intuitu Dei et pro salute anime nostre et omnium antecessorum et heredum nostrorum ad honorem Dei et exaltacionem sancte Ecclesie, et emendacionem regi nostri, per consilium venerabilium patrum nostrorum, Stephani Cantuariensis archiepsicopi, tocius Anglie primatis et sancte Romane ecclesie cardinalis, Henrici Dublinensis archiepiscopi, Willelmi Londoniensis, Petri Wintoniensis, Joscelini Bathoniensis et Glastoniensis, Hugonis Lincolniensis, Walteri Wygorniensis, Willelmi Coventriensis, et Benedicti Roffensis, episcoporum; magistri Pandulfi domini pape subdiaconi et familiaris, fratris Aymerici magistri milicie Templi in Anglia; et nobilium virorum Willelmi Mariscalli comitis Penbrocie, Willelmi comitis Sarisberie, Willelmi comitis Warennie, Willelmi comitis Arundellie, Alani de Galewey a constabularii Scocie, Warini filii Geroldi, Petri filii Hereberti, Huberti de Burgo senescalli Pictavie, Hugonis de Nevilla, Mathei filii Hereberti, Thome Basset, Alani Basset, Philippi de Albiniaco, Roberti de Roppel, Johannis Mariscalli, Johannis filii Hugonis et aliorum fidelium nostrum.",
                               "Les représentants du peuple français, constitués en Assemblée nationale, considérant que l'ignorance, l'oubli ou le mépris des droits de l'homme sont les seules causes des malheurs publics et de la corruption des gouvernements, ont résolu d'exposer, dans une déclaration solennelle, les droits naturels, inaliénables et sacrés de l'homme, afin que cette déclaration, constamment présente à tous les membres du corps social, leur rappelle sans cesse leurs droits et leurs devoirs ; afin que les actes du pouvoir législatif et ceux du pouvoir exécutif, pouvant être à chaque instant comparés avec le but de toute institution politique, en soient plus respectés ; afin que les réclamations des citoyens, fondées désormais sur des principes simples et incontestables, tournent toujours au maintien de la Constitution et au bonheur de tous.",
                               "We the People of the United States, in Order to form a more perfect Union, establish Justice, insure domestic Tranquility, provide for the common defence, promote the general Welfare, and secure the Blessings of Liberty to ourselves and our Posterity, do ordain and establish this Constitution for the United States of America."],
                   'query': "Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal. Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this. But, in a larger sense, we can not dedicate -- we can not consecrate -- we can not hallow -- this ground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember what we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remaining before us -- that from these honored dead we take increased devotion to that cause for which they gave the last full measure of devotion -- that we here highly resolve that these dead shall not have died in vain -- that this nation, under God, shall have a new birth of freedom -- and that government of the people, by the people, for the people, shall not perish from the earth."}

        return results
