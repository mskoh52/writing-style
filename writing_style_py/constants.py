import os
from sqlalchemy import create_engine

hostname = os.uname().nodename
if hostname == 'hormiga':
    username = 'mkoh'
    datadir = '/mnt/data/writing-style'
    outputdir = '/home/mkoh/projects/writing-style/data'
elif hostname == 'msklaptop':
    username = 'mkoh'
    datadir = '/home/mkoh/Documents/projects/writing-style/data'
    outputdir = '/home/mkoh/Documents/projects/writing-style/data'
elif hostname == 'authorcheck':
    username = 'ubuntu'
    datadir = '/home/ubuntu/writing-style/data'
    outputdir = '/home/ubuntu/writing-style/data'


dbname = 'gutenberg'
dbengine = create_engine('postgres://{}@localhost/{}'.format(username,dbname))
