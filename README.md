# Author Check: a tool for analyzing documents for stylistic similarity

This is Author Check. It is a tool for attributing authorship to unknown
documents. You can pass in a writing sample of known authorship and a query
document, and it will try to determine whether or not the query was written by
the same author as the sample.

The demo is available at [authorcheck.kohkodes.com](http://authorcheck.kohkodes.com),
and there is some information about how it all works
at [this blog post](http://www.kohkodes.com/posts/2017-06-26-authorcheck/).

**Sections**:
* [`writing_style_py`](#writing_style_py): Python code to preprocess data, compute features, and
  calculate authorship, plus some other loose ends
* [`frontend`](#frontend): Flask app to demonstrate the project
* [`blogpost`](#blogpost): Blog-style writeup of the project and static version of demo

## `writing_style_py`

Python module that implements file preprocessing, feature extraction and anomaly
detection. Below are instructions for the minimum steps necessary to get the web
frontend up and running.

### Setup

We need to create a location to store the data and point the Python module there. Create
a folder somewhere to store the raw text files and catalog from Project Gutenberg:

    export DATADIR=/path/to/data/
    mkdir $DATADIR

We also need a place to store the output of the module (intermediate files, etc.):

    export OUTPUTDIR=/path/to/output/
    mkdir $OUTPUTDIR

Finally, we need to set up a PostgreSQL database to store the processed data.
After installing Postgres, create the database with:

    createdb gutenberg

Now edit `writing_style_py/constants.py` and update with your username and the
paths you specified above.

### Getting the data

Throughout this process, you should be in `$DATADIR`:

    cd $DATADIR

First, obtain the Project Gutenberg catalog:

    wget https://www.gutenberg.org/cache/epub/feeds/rdf-files.tar.zip
    unzip rdf-files.tar.zip

Next run the script to parse the catalog and obtain metadata info:

    python -m writing_style_py.preprocessing.parse_catalog

This will create a pickled `pandas.DataFrame` with the metadata for each book.

Next, create the list of URLs that we want to download. For now, we limit
ourselves to the ten most prolific authors on the site.

    python -m writing_style_py.preprocessing.download_top_authors
    mkdir top10 && cd top10
    wget -i top_author_urls.txt
    cd ..

Finally, we split the documents into chunks and load them into our databaswe:

    python -m writing_style_py.preprocessing.extract_text

### Computing features

    python -m writing_style_py.features.compute_features

The `analysis` submodule, appropriately, performs some analysis on these
features. The only one that is required is fitting a linear discriminant
analysis model.

    python -m writing_style_py.features.analysis -t lda

For fun, you can also fit a logistic regression with and without PCA
dimensionality reduction, and make a t-SNE visualization of the data

    python -m writing_style_py.features.analysis -t lr      # fit logistic regression
    python -m writing_style_py.features.analysis -t lr_pca  # fit logistic regression after doing different amounts of dimensionality reduction with PCA
    python -m writing_style_py.features.analysis -t tsne    # Visualize the data using t-SNE
                                                            # Depends on MulticoreTSNE from https://github.com/DmitryUlyanov/Multicore-TSNE/

### Train the anomaly detector

We need to now find the threshold for our anomaly detector.

    python -m writing_style_py.classifier.anomaly

With this done, we can run the web app frontend.

## `frontend`

Full version of the authorship detector in the form of a Flask app. To run the
app locally, first `git clone` the entire repo, add the `writing_style_py`
module to your PYTHONPATH and launch `run.py`. For example:

    git clone https://github.com/mskoh52/writing-style.git
    export PYTHONPATH=$(pwd)/writing-style
    cd writing-style && ./run.py

## `blogpost`

Folder contains a markdown-based writeup of the project and a static version of
the demo site.
