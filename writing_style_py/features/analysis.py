from ..constants import dbengine, outputdir
from ..log_setup import setup_logging
from .compute_features import count_dicts_to_sparse, count_char_ngrams, spacy_parse_docs
from .ngram import NGramCounter
from MulticoreTSNE import MulticoreTSNE as TSNE
from scipy.sparse import csr_matrix
from sklearn.decomposition import TruncatedSVD
from sklearn.decomposition.pca import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, accuracy_score, f1_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
import argparse
import logging
import numpy as np
import os
import pandas as pd
import pickle
import scipy.sparse

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

logger = setup_logging(__name__, mode='a', level=logging.DEBUG)

def visualize_tsne(embedding, authors):
    # TODO d3.js vis where you can pick authors to highlight
    #      unpicked authors are gray dots
    authors = np.array(authors)
    u_authors = np.sort(np.unique(authors))
    author_inds = []
    for a in u_authors:
        author_inds.append(np.argwhere(authors == a))

    markers = ['o', '^', 's', 'x', '*'] * 2
    colors = sns.hls_palette(10, l=.6, s=.5)

    for inds, author, marker, color in zip(author_inds, u_authors, markers, colors):
        sample = np.random.choice(inds.flatten(), min(len(inds), 2000), replace=False)
        plt.scatter(embedding[sample, 0],
                    embedding[sample, 1],
                    label=author,
                    marker=marker,
                    color=color,
                    alpha=.3)

    plt.gca().set_xticks([])
    plt.gca().set_yticks([])
    plt.legend()
    return plt.gcf()

def compare_authors(x1, x2, set1_inds, set2_inds, set3_inds, set1_label, set2_label, set3_label):
    """Takes three sets of documents + three author names and displays a scatter
    plot of avg word length vs avg sentence length

    """
    sample1 = np.random.choice(set1_inds, 2000, replace=False)
    sample2 = np.random.choice(set2_inds, 2000, replace=False)
    sample3 = np.random.choice(set3_inds, 2000, replace=False)

    plt.plot(x1[sample1], x2[sample1], marker='.', markersize=15, linestyle='none', color='b', alpha=.1, label=set1_label)
    plt.plot(x1[sample2], x2[sample2], marker='.', markersize=15, linestyle='none', color='g', alpha=.1, label=set2_label)
    plt.plot(x1[sample3], x2[sample3], marker='.', markersize=15, linestyle='none', color='#990000', alpha=.1, label=set3_label)
    plt.legend()
    plt.gca().set_xlabel('Average sentence length')
    plt.gca().set_ylabel('Average word length')
    return plt.gcf()

class FeatureAnalysis():
    def __init__(self,
                 input_table='chunks',
                 word_n_highest=200,
                 unigram_n_highest=100,
                 trigram_n_highest=100,
                 pentagram_n_highest=200,
                 feature_file=None,
                 colnames_file=None):

        self.input_table = input_table
        self.word_n_highest = word_n_highest
        self.unigram_n_highest = unigram_n_highest
        self.trigram_n_highest = trigram_n_highest
        self.pentagram_n_highest = pentagram_n_highest

        # Cache
        self.reset_cache()

        logger.info('Beginning new analysis with following params:')
        logger.info('input_table = {}'.format(self.input_table))
        logger.info('word_n_highest = {}'.format(self.word_n_highest))
        logger.info('unigram_n_highest = {}'.format(self.unigram_n_highest))
        logger.info('trigram_n_highest = {}'.format(self.trigram_n_highest))
        logger.info('pentagram_n_highest = {}'.format(self.pentagram_n_highest))

        if feature_file:
            logger.info('loading features from disk'.format(self.pentagram_n_highest))
            self.get_features(feature_file=feature_file, colnames_file=colnames_file)

    def reset_cache(self):
        self.feature_matrix = None
        self.colnames = None
        self.docs = None

    def load_count_matrix(self, table_name):
        """ Load word counts from database

        Parameters
        ----------
        table_name : str
            name of the count table

        Returns
        -------
        lut : dict
            Mapping from matrix column indices to symbols
        counts : scipy.sparse.csr_matrix
            Count matrix
        """
        lut = dict([(x.col, x.symbol) for x in pd.read_sql_table(table_name + '_lut', dbengine).itertuples()])
        counts = pd.read_sql_table(table_name, dbengine, index_col='index').sort_index()
        counts = scipy.sparse.vstack(pickle.loads(x) for x in counts.symbol_counts)
        return lut, counts

    def select_top_counts(self, lut, counts, n_highest, colnames_dict=None):
        """ Select top ranking counts

        Parameters
        ----------
        lut : list
            List of symbols which determines the order of
            columns in the output matrix
        counts : scipy.sparse.csr_matrix
            Count matrix
        n_highest : int
            how many to keep
        colnames_dict: dict
            Mapping from colnames -> inds in final feature matrix.
            Used to match previously computed feature columns to new
            ones.

        Returns
        -------
        symbols : list of str
            list of symbols in new reduced matrix
        counts : scipy.sparse.csr_matrix
            matrix containing only the top ranked columns
        """
        if colnames_dict:
            inds = [n for n, x in enumerate(lut) if x in colnames_dict]
        else:
            inds = np.argsort(np.asarray(counts.sum(axis=0)).flatten())[-n_highest:]

        symbols = [lut[i] for i in inds]
        counts = counts[:, inds]
        return symbols, counts

    def get_features(self, feature_file=None, colnames_file=None):
        if self.feature_matrix is None:
            def get_subtable(name):
                return self.input_table + '_' + name

            logger.debug('loading documents')
            docs = pd.read_sql_table(self.input_table, dbengine, index_col='index').sort_index()
            self.docs = docs

            if feature_file:
                self.feature_matrix = pickle.load(open(feature_file, 'rb'))
                self.colnames = pickle.load(open(colnames_file, 'rb'))
            else:
                logger.debug('loading numeric features')
                features = pd.read_sql_table(get_subtable('features'), dbengine, index_col='index').sort_index().drop('length', axis=1)
                feature_names = list(features.columns)
                features = csr_matrix(features)

                logger.debug('loading word counts')
                words, word_count = self.select_top_counts(*self.load_count_matrix(get_subtable('word_count')), self.word_n_highest)
                logger.debug('loading unigram counts')
                unigrams, unigram_count = self.select_top_counts(*self.load_count_matrix(get_subtable('1gram_count')), self.unigram_n_highest)
                logger.debug('loading trigram counts')
                trigrams, trigram_count = self.select_top_counts(*self.load_count_matrix(get_subtable('3gram_count')), self.trigram_n_highest)
                logger.debug('loading pentagram counts')
                pentagrams, pentagram_count = self.select_top_counts(*self.load_count_matrix(get_subtable('5gram_count')), self.pentagram_n_highest)

                colnames = dict(zip(range(0, len(feature_names + words + unigrams + trigrams + pentagrams)),
                                    feature_names + words + unigrams + trigrams + pentagrams))
                self.colnames = colnames

                feature_matrix = scipy.sparse.hstack((features, word_count, unigram_count, trigram_count, pentagram_count)).tocsr()
                self.feature_matrix = feature_matrix

        return self.feature_matrix, self.colnames, self.docs

    def compute_new_features(self, new_docs, nlp, old_lut, colnames_dict):
        """Takes new documents and returns feature matrix and column names. Uses `nlp`
        and `colnames_dict` arguments to ensure that the word count and ngram counts are
        calculated using the same underlying set of words/ngrams

        Parameters
        ----------
        new_docs : pandas.DataFrame
            New documents/chunks to compute features for
        nlp : spacy.en.English
            spacy model whose nlp.vocab.strings attribute matches
            colnames_dict['word'] contents
        old_lut : dict of set
            dict with fields ['word', 'unigram', 'trigram', 'pentagram'],
            containing sets of words/ngrams in previously computed feature
            set
        colnames_dict : dict of dict
            dict with fields ['feature', 'word', 'unigram', 'trigram',
            'pentagram'], whose corresponding values are dict-like objects
            mapping symbols to indices in order to make the new column names
            match the old ones

        Returns
        -------
        new_feature_matrix : scipy.sparse.csr_matrix
        new_colnames : list of str

        """
        def reorder_columns(matrix, colnames, inds):
            # take matrix, column names, and dict of colnames -> inds
            # and rearrange the columns
            start = min(inds.values())
            target_cols = [inds[x] - start for x in colnames]
            sort_order = [x[0] for x in sorted(enumerate(target_cols), key=lambda x: x[1])]
            new_colnames = [colnames[i] for i in sort_order]
            new_matrix = matrix[:, sort_order]
            return new_matrix, new_colnames

        print('\nGet numerical features and word counts\n')
        features, feature_names = spacy_parse_docs(nlp, new_docs)

        word_counters = [NGramCounter(counts=wc) for wc in features.word_count]
        word_counters[0].reconcile(old_lut['word'])
        mat, hashes = count_dicts_to_sparse(features.word_count)
        names = [nlp.vocab.strings[int(h)] for h in hashes]
        words, word_count = self.select_top_counts(names, mat, self.word_n_highest, colnames_dict['word'])
        word_count, words = reorder_columns(word_count, words, colnames_dict['word'])

        # Drop extraneous/unused columns
        features = features.drop(['word_count', 'length'], axis=1)
        feature_names = list(features.columns)
        features = csr_matrix(features)

        # Reorder feature columns
        features, feature_names = reorder_columns(features, feature_names, colnames_dict['feature'])

        print('\nCount character 1grams\n')
        unigram_counters = count_char_ngrams(new_docs, 1)
        unigram_counters[0].reconcile(old_lut['unigram'])
        mat, names = count_dicts_to_sparse([x.counts for x in unigram_counters])
        unigrams, unigram_count = self.select_top_counts(names, mat, self.unigram_n_highest, colnames_dict['unigram'])
        unigram_count, unigrams = reorder_columns(unigram_count, unigrams, colnames_dict['unigram'])

        print('\nCount character 3grams\n')
        trigram_counters = count_char_ngrams(new_docs, 3)
        trigram_counters[0].reconcile(old_lut['trigram'])
        mat, names = count_dicts_to_sparse([x.counts for x in trigram_counters])
        trigrams, trigram_count = self.select_top_counts(names, mat, self.trigram_n_highest, colnames_dict['trigram'])
        trigram_count, trigrams = reorder_columns(trigram_count, trigrams, colnames_dict['trigram'])

        print('\nCount character 5grams\n')
        pentagram_counters = count_char_ngrams(new_docs, 5)
        pentagram_counters[0].reconcile(old_lut['pentagram'])
        mat, names = count_dicts_to_sparse([x.counts for x in pentagram_counters])
        pentagrams, pentagram_count = self.select_top_counts(names, mat, self.pentagram_n_highest, colnames_dict['pentagram'])
        pentagram_count, pentagrams = reorder_columns(pentagram_count, pentagrams, colnames_dict['pentagram'])
        print('\ndone\n')

        new_colnames = dict(zip(range(0, len(feature_names + words + unigrams + trigrams + pentagrams)),
                                feature_names + words + unigrams + trigrams + pentagrams))
        new_feature_matrix = scipy.sparse.hstack((features, word_count, unigram_count, trigram_count, pentagram_count)).tocsr()

        return new_feature_matrix, new_colnames

def run_logistic_regression(features, docs, shuffle_labels=False, pca_dims=None):
    encoder = LabelEncoder()
    labels = encoder.fit_transform(docs.author)
    if shuffle_labels:
        np.random.shuffle(labels)

    if pca_dims:
        pca = PCA(n_components=pca_dims)
        features = pca.fit_transform(features)

    X_train, X_test, y_train, y_test = train_test_split(features,
                                                        labels,
                                                        test_size=.2,
                                                        random_state=9,
                                                        stratify=labels)

    logger.debug('fitting logistic regression')
    lr = LogisticRegression(C=1,
                            multi_class='multinomial',
                            solver='lbfgs',
                            max_iter=1000,
                            verbose=1,
                            random_state=9)

    lr.fit(X_train, y_train)
    y_pred = lr.predict(X_test)

    cm = confusion_matrix(y_test, y_pred)
    acc = accuracy_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred, average='micro')

    logger.info('\n' + classification_report(y_test,
                                             y_pred,
                                             labels=np.unique(labels),
                                             target_names=encoder.inverse_transform(np.unique(labels))))
    logger.info('Accuracy: {}'.format(acc))
    logger.info('F1: {}'.format(f1))
    logger.info('Confusion matrix:\n{}'.format(cm))

    return lr, acc, f1, cm

def run_lda(features, docs, shuffle_labels=False):
    encoder = LabelEncoder()
    labels = encoder.fit_transform(docs.author)
    if shuffle_labels:
        np.random.shuffle(labels)

    X_train, X_test, y_train, y_test = train_test_split(features,
                                                        labels,
                                                        test_size=.2,
                                                        random_state=9,
                                                        stratify=labels)

    logger.debug('fitting linear discriminant analysis')
    lda = LinearDiscriminantAnalysis()

    lda.fit(X_train, y_train)
    y_pred = lda.predict(X_test)

    cm = confusion_matrix(y_test, y_pred)
    acc = accuracy_score(y_test, y_pred)
    f1 = f1_score(y_test, y_pred, average='micro')

    logger.info('\n' + classification_report(y_test,
                                             y_pred,
                                             labels=np.unique(labels),
                                             target_names=encoder.inverse_transform(np.unique(labels))))
    logger.info('Accuracy: {}'.format(acc))
    logger.info('F1: {}'.format(f1))
    logger.info('Confusion matrix:\n{}'.format(cm))

    return lda, acc, f1, cm

def tsne2d(features, docs, seed, pca_first=True, *args, **kwargs):
    """
    Parameters
    ----------
    features
        inputs in high-D feature space
    docs
        input documents
    seed : int
        seed for numpy RNG
    *args, **kwargs
        args for TSNE (see sklearn docs)
    Returns
    -------
    flat : np.array
        2d t-sne embedding
    indices
        indices of the subset of documents used
    """
    np.random.seed(seed)
    shuffle = np.arange(features.shape[0])
    np.random.shuffle(shuffle)

    features_subset = features[shuffle,:]
    features_subset = features_subset[:30000,:]

    docs_subset = docs.iloc[shuffle]
    docs_subset = docs_subset.iloc[:30000]

    if pca_first:
        tsvd = TruncatedSVD(n_components=100)
        features_r = tsvd.fit_transform(features_subset)
    else:
        features_r = features_subset

    tsne = TSNE(*args, **kwargs)
    flat = tsne.fit_transform(features_r)

    return flat, docs_subset.index

def load_old_colnames(colnames_file, n_feature, n_word, n_unigram, n_trigram, n_pentagram):
    colnames = pickle.load(open(colnames_file, 'rb'))
    colnames_l = [x[1] for x in sorted(colnames.items(), key=lambda x: x[0])]
    colnames_dict = {}

    ind = 0
    colnames_dict['feature'] = dict(zip(colnames_l[ind:ind+n_feature], range(ind,ind+n_feature)))
    ind += n_feature
    colnames_dict['word'] = dict(zip(colnames_l[ind:ind+n_word], range(ind,ind+n_word)))
    ind += n_word
    colnames_dict['unigram'] = dict(zip(colnames_l[ind:ind+n_unigram], range(ind,ind+n_unigram)))
    ind += n_unigram
    colnames_dict['trigram'] = dict(zip(colnames_l[ind:ind+n_trigram], range(ind,ind+n_trigram)))
    ind += n_trigram
    colnames_dict['pentagram'] = dict(zip(colnames_l[ind:ind+n_pentagram], range(ind,ind+n_pentagram)))

    return colnames_dict

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--task', action='append')
    args = parser.parse_args()
    tasks = args.task
    if not tasks:
        tasks = ['lr', 'lda', 'tsne', 'lr_pca']

    analysis = FeatureAnalysis(input_table='chunks',
                               word_n_highest=200,
                               unigram_n_highest=100,
                               trigram_n_highest=100,
                               pentagram_n_highest=200)

    features, colnames, docs = analysis.get_features()
    os.makedirs(os.path.join(outputdir, 'features'), exist_ok=True)
    pickle.dump(features, open(os.path.join(outputdir, 'features', 'features_200-100-100-200.pkl'), 'wb'))
    pickle.dump(colnames, open(os.path.join(outputdir, 'features', 'colnames_200-100-100-200.pkl'), 'wb'))
    pickle.dump(docs, open(os.path.join(outputdir, 'features', 'docs.pkl'), 'wb'))

    if 'lr' in tasks:
        os.makedirs(os.path.join(outputdir, 'classifier'), exist_ok=True)

        lr, acc, f1, cm = run_logistic_regression(features, docs)
        pickle.dump(lr, open(os.path.join(outputdir, 'classifier', 'lr_model.pkl'), 'wb'))
        pickle.dump({'acc': acc, 'f1': f1, 'cm': cm}, open(os.path.join(outputdir, 'classifier', 'lr_results.pkl'), 'wb'))

        lr_s, acc_s, f1_s, cm_s = run_logistic_regression(features, docs, shuffle_labels=True)
        pickle.dump(lr_s, open(os.path.join(outputdir, 'classifier', 'lr_shuffledlabels_model.pkl'), 'wb'))
        pickle.dump({'acc': acc, 'f1': f1, 'cm': cm}, open(os.path.join(outputdir, 'classifier', 'lr_shuffledlabels_results.pkl'), 'wb'))

    if 'lda' in tasks:
        os.makedirs(os.path.join(outputdir, 'classifier'), exist_ok=True)

        lda, acc, f1, cm = run_lda(features.todense(), docs)
        pickle.dump(lda, open(os.path.join(outputdir, 'classifier', 'lda_model.pkl'), 'wb'))
        pickle.dump({'acc': acc, 'f1': f1, 'cm': cm}, open(os.path.join(outputdir, 'classifier', 'lda_results.pkl'), 'wb'))

    if 'tsne' in tasks:
        for p in [5, 10, 30, 50, 100]:
            flat, inds = tsne2d(features, docs, perplexity=p, n_jobs=8, n_iter=1500)

            pickle.dump(flat, open(os.path.join(outputdir, 'tsne_{}.pkl').format(p), 'wb'))
            pickle.dump(list(docs.iloc[inds].author), open(os.path.join(outputdir, 'tsne_authors_{}.pkl').format(p), 'wb'))

            fig = visualize_tsne(flat, list(docs.iloc[inds].author))
            fig.savefig(os.path.join(outputdir, 'tsne_{}.png').format(p))
            plt.close('all')

    if 'lr_pca' in tasks:
        os.makedirs(os.path.join(outputdir, 'classifier'), exist_ok=True)

        for ndim in [2, 5, 9, 50, 100, 200]:
            logger.info('running logistic regression with first {} prin comps'.format(ndim))
            lr, acc, f1, cm = run_logistic_regression(features, docs, pca_dims=ndim)
            pickle.dump(lr_s, open(os.path.join(outputdir, 'classifier', 'lr_pca{}_model.pkl'.format(ndim)), 'wb'))
            pickle.dump({'acc': acc, 'f1': f1, 'cm': cm}, open(os.path.join(outputdir, 'classifier', 'lr_pca{}_results.pkl'.format(ndim)), 'wb'))
