# from pystan import StanModel
import numpy as np
# import matplotlib.pyplot as plt
import pickle
import pystan
from hashlib import md5

def stan_cache(file, model_name=None, **kwargs):
    """Use just as you would `stan`"""
    with open(file, 'r') as f:
        model_code = f.read()
    code_hash = md5(model_code.encode('ascii')).hexdigest()
    if model_name is None:
        cache_fn = 'cached-model-{}.pkl'.format(code_hash)
    else:
        cache_fn = 'cached-{}-{}.pkl'.format(model_name, code_hash)
    try:
        sm = pickle.load(open(cache_fn, 'rb'))
    except:
        sm = pystan.StanModel(model_code=model_code)
        with open(cache_fn, 'wb') as f:
            pickle.dump(sm, f)
    else:
        print("Using cached StanModel")
    return sm.sampling(**kwargs)

if __name__ == '__main__':
    M = 100
    N = 3
    mu = np.random.randn(N)
    print(mu)
    sigma = np.eye(N)
    data = {'M': M,
            'N': N,
            'X': np.random.multivariate_normal(mu, sigma, size=M),
            'y': np.random.multivariate_normal(mu, sigma)}

    def initialize():
        mu = np.mean(data['X'], axis=0)
        omega = np.zeros((N,N))
        sigma = np.var(data['X'], axis=0)
        return {'mu': mu, 'omega': omega, 'sigma': sigma}

    fit = stan_cache(file='normal_flatprior.stan',
                     model_name='normal_flatprior',
                     data=data,
                     init=initialize,
                     warmup=2000,
                     iter=12000,
                     chains=3)
    print(fit)
