import logging
from os.path import abspath, dirname, join as joinpath
from os import makedirs

def setup_logging(logname, mode='w', level=logging.INFO):
    logdir = abspath(joinpath(dirname(__file__), './logs'))
    makedirs(logdir, exist_ok=True)

    fmt = logging.Formatter(fmt='%(name)s, %(asctime)s : %(levelname)s : %(message)s')

    file_handler = logging.FileHandler(joinpath(logdir, logname + '.log'), mode=mode)
    file_handler.setFormatter(fmt)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(fmt)

    logger = logging.getLogger(logname)
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)
    logger.setLevel(level)

    return logger

def getLogger(zz):
    print('hi')
