from ..constants import datadir, dbengine
from ..log_setup import setup_logging
import os
import pandas as pd
import numpy as np
import re
import glob
import nltk
import multiprocessing

logger = setup_logging(__name__)

def load_document(filename):
    try:
        with open(filename, 'r', encoding='utf-8') as f:
            doc = f.read()
    except UnicodeDecodeError:
        try:
            with open(filename, 'r', encoding='iso-8859-1') as f:
                doc = f.read()
        except UnicodeDecodeError:  # TODO wtf?
            with open(filename, 'r', encoding='iso-8859-1') as f:
                doc = f.read()
    return doc

def get_language(document):
    lang = re.search('Language:(.*)', document)
    return lang.group(1).strip() if lang else None

def extract_content(document: str) -> str:
    def extract_00(document: str) -> str:
        start_flag = re.search('\*+ *START OF.*?\*+', document, re.DOTALL)
        end_flag = re.search('\*+ *END OF.*?\*+', document, re.DOTALL)
        assert start_flag, 'START OF flag not found'
        assert end_flag, 'END OF flag not found'
        start = start_flag.span()[1]+1
        end = end_flag.span()[0]
        return document[start:end]

    def extract_01(document: str) -> str:
        small_print_inds = list(re.finditer('\<\<THIS.*?\>\>', document, re.DOTALL))
        assert small_print_inds, '<<THIS.*?>> message not found'
        start = small_print_inds[1].span()[1]+1
        end = small_print_inds[-1].span()[0]
        return re.sub('\<\<THIS.*?\>\>', '', document[start:end], count=0, flags=re.DOTALL)

    def extract_02(document: str) -> str:
        start_flag = re.search('^[A-Z ]+$', document, re.MULTILINE)
        end_flag = re.search("ETEXT EDITOR'S BOOKMARKS", document)
        # end_flag = re.search('^\*+\nThe Project Gutenberg.*\n\*+This file should.*', document, re.MULTILINE)
        assert start_flag, 'All caps title not found'
        assert end_flag, "ETEXT EDITOR'S BOOKMARKS flag not found"
        start = start_flag.span()[0]
        end = end_flag.span()[0]
        return document[start:end]

    def extract_03(document: str) -> str:
        start_flag = re.search('^[A-Z ]+$', document, re.MULTILINE)
        end_flag = re.search('End of Project Gutenberg Etext of .*', document)
        assert start_flag, 'All caps title not found'
        assert end_flag, 'End of Project... ending flag not found'
        start = start_flag.span()[1]+1
        end = end_flag.span()[0]
        return document[start:end]

    def extract_04(document:str) -> str:
        start_flag = re.search('David Reed\n', document, re.MULTILINE)
        assert start_flag, '"David Reed" not found'
        start = start_flag.span()[1]+1
        return document[start:]

    if get_language(document) != 'English' and get_language(document) is not None:
        return ''

    success = False
    for n, extractor in enumerate([extract_00, extract_01, extract_02, extract_03, extract_04]):
        try:
            doc = extractor(document)
            success = True
            break
        except Exception as e:
            pass
            # print(str(e))
    if not success:
        return 'WARNING NO DELIMITERS FOUND WARNING NO DELIMITERS FOUND WARNING NO DELIMITERS FOUND' + document

    return doc.replace('\n', ' ') \
              .replace('”', '"') \
              .replace('’', "'") \
              .replace('“', '"') \
              .replace('‘', "'")

def get_top10_author_documents(limit = None, seed = None):
    """ Get top10 author documents

    Parameters
    ----------
    limit : int, optional
        truncate both documents and chunks to this many rows
    seed : int, optional
        random seed to use for choosing rows when limit is used

    Outputs
    -------
    documents : pandas.DataFrame
        dataframe with columns (id, author, title, urls, text)
    """
    if limit:
        if seed:
            np.random.seed(seed)

    info = pd.read_pickle(os.path.join(datadir, 'gutenberg_info.pkl'))

    not_real_authors = {'Various', 'Anonymous', 'Unknown'}
    author_counts = info.groupby('author').count().iloc[:,0].sort_values(ascending=False)
    real_authors = author_counts.reset_index().apply(lambda x: x.author not in not_real_authors, axis=1)
    author_counts = author_counts.iloc[np.argwhere(real_authors).flatten()]
    top_authors = author_counts.iloc[:10].index
    documents = info.loc[info.apply(lambda x: x.author in top_authors, axis=1)].copy()

    try:
        files = glob.glob(os.path.join(datadir, 'top10', '*.txt*'))
        assert(len(files) > 0)
    except AssertionError:
        logger.error('No files found. Did you download them?')

    if limit:
        inds = np.random.randint(0, len(files), size=(limit,))
        files = [f for n,f in enumerate(files) if n in inds]

    documents['text'] = np.full((len(documents)), np.nan)
    for n,f in enumerate(files):
        if n % 100 == 0:
            print('Loading document {}/{}'.format(n+1, len(files)), end='\r')
        # TODO: choose files by going down an ordered list of possible name formats
        ix = np.argwhere(
            documents['id'].map(lambda x: os.path.basename(str(x))) == re.match('([a-z0-9]+)-*[0-9]{,1}\.txt', os.path.basename(f)).group(1)).flatten()
        documents.iloc[ix[0], -1] = extract_content(load_document(f))

    documents = documents.replace('', np.nan).dropna().reset_index(drop=True)

    return documents

def get_sentences(documents):
    documents = documents.copy()
    sentences = []
    try:
        pool = multiprocessing.Pool(8)
        for n, sent in enumerate(pool.imap(nltk.sent_tokenize, documents.text)):
            print('parsed {}/{}'.format(n+1, len(documents)), end='\r')
            sentences.append(sent)
        documents['sentences'] = sentences
    finally:
        pool.close()
        pool.join()
    return documents

def chunk_document(sentences, target_char_len):
    chunks = []
    current_chunk = ''
    count = 0
    for s in sentences:
        current_chunk += ' ' + str(s)
        count += len(str(s))
        if count > target_char_len:
            chunks.append(current_chunk)
            current_chunk = ''
            count = 0
    return chunks

def get_chunks(documents, chunksize=1000, limit=None, seed=None):
    """Get chunks from documents

    Parameters
    ----------
    documents : pandas.DataFrame
        dataframe with columns (id, author, title, urls, text)
    chunksize : int, optional
        Approximate number of characters in each chunk
    limit : int, optional
        truncate both documents and chunks to this many rows
    seed : int, optional
        random seed to use for choosing rows when limit is used

    Outputs
    -------
    chunks : pandas.DataFrame
        dataframe with columns (id, author, title, text, document_index).
        `document_index` is the index into the documents table
    """
    documents = documents.copy()
    documents['chunks'] = documents['sentences'].map(lambda doc: chunk_document(doc, 1000))
    chunks = documents.drop('text', axis=1)
    chunks = chunks.join(
        pd.concat([pd.DataFrame(v, columns=['text'], index=np.repeat(k, len(v))) for k,v in chunks.chunks.to_dict().items()])
    ).drop(['sentences', 'chunks', 'urls'], axis=1).reset_index()
    chunks['document_index'] = chunks['index']
    chunks.drop('index', axis=1, inplace=True)
    if limit:
        inds = np.random.randint(0, len(chunks), size=(limit,))
        chunks = chunks.iloc[inds]
    return chunks

if __name__ == '__main__':
    documents = get_top10_author_documents()

    print('getting chunks')
    chunks = get_chunks(get_sentences(documents), chunksize=1000)
    print('got {} chunks'.format(len(chunks)))
    documents.to_sql('documents', dbengine, if_exists='replace')
    chunks.to_sql('chunks', dbengine, if_exists='replace')
