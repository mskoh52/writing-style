# todo: handle extra url special cases
#       use the generator, filter for txt as last resort
from ..constants import datadir
from .log_setup import setup_logging
import os
import multiprocessing as mp
import glob
import rdflib
from rdflib import Namespace
import pandas as pd
import pickle

logger = setup_logging(__name__)

def get_info(filename):
    g = rdflib.Graph()
    g.load(filename)
    purl = Namespace('http://purl.org/dc/terms/')
    pgterms = Namespace('http://www.gutenberg.org/2009/pgterms/')

    try:
        id_ = next(g.triples((None, purl.isFormatOf, None)))[-1]
    except:
        id_ = None

    try:
        author = next(g.triples((None, pgterms.name, None)))[-1].value
    except:
        author = None

    try:
        title = g.value(id_, purl.title).value
    except:
        title = None

    urls = []
    for s,p,o in g.triples((None, purl.isFormatOf, None)):
        if str(s).find('.txt') >= 0:
            urls.append(str(s))

    return id_, author, title, urls

if __name__ == '__main__':
    catalog_dir = 'cache/epub'
    files = glob.glob(os.path.join(datadir, catalog_dir, '*/*.rdf'))

    pool = mp.Pool(8)
    info = []
    try:
        for n, i in enumerate(pool.imap(get_info, files, chunksize=10)):
            info.append(i)
            if (n+1) % 10 == 0:
                print('progress: {0} / {1}'.format(n+1, len(files)), end='\r')
    finally:
        pool.close()
        pool.join()

    df = pd.DataFrame(info, columns=('id', 'author', 'title', 'urls'))
    df.to_pickle(os.path.join(datadir, 'gutenberg_info.pkl'))
