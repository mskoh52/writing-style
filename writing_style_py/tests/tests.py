from ..constants import dbengine
from ..preprocessing.extract_text import get_top10_author_documents, get_chunks, get_sentences
from sqlalchemy import create_engine
import scipy.sparse
import numpy as np
import pandas as pd
import pickle
import re
import unittest
import spacy

class TestSqlWriteFromScratch(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dbengine = create_engine('postgres://{}@localhost/{}'.format('mkoh', 'testdb'))
        documents = get_top10_author_documents(limit=100, seed=9)
        chunks = get_chunks(get_sentences(documents), limit=100, seed=9)
        documents.to_sql('documents', self.dbengine, if_exists='replace')
        chunks.to_sql('chunks', self.dbengine, if_exists='replace')

    def test_documents(self):
        documents = pd.read_sql_query('SELECT * FROM documents', self.dbengine, index_col='index')
        cols = ['id', 'author', 'title', 'urls', 'text']
        self.assertEqual(len(documents.columns), len(cols))

        self.assertEqual(type(documents.iloc[0].author), str)
        self.assertEqual(type(documents.iloc[0].title), str)
        self.assertEqual(type(documents.iloc[0].text), str)

        for c,cc in zip(documents.columns, cols):
            self.assertEqual(c, cc)

    def test_chunks(self):
        chunks = pd.read_sql_query('SELECT * FROM chunks', self.dbengine, index_col='index')
        cols = ['id', 'author', 'title', 'text', 'document_index']
        self.assertEqual(len(chunks.columns), len(cols))

        self.assertEqual(type(chunks.iloc[0].author), str)
        self.assertEqual(type(chunks.iloc[0].title), str)
        self.assertEqual(type(chunks.iloc[0].text), str)

        for c,cc in zip(chunks.columns, cols):
            self.assertEqual(c, cc)

class TestFeatureIndexing(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.docs = pd.read_sql_table('chunks', dbengine, index_col='index')

    def test_id_correct(self):
        # test that indices are correct by making sure that computed length
        # from features calculator equals the computed length from the raw text
        query = """
        SELECT bool_and(l1=l2)
        FROM (SELECT chunks.index, length(text) AS l1, chunks_features.length AS l2 FROM chunks
              INNER JOIN chunks_features ON (chunks_features.index = chunks.index)) AS test;
        """
        self.assertTrue(dbengine.execute(query).fetchall()[0][0])

    def test_word_count(self):
        nlp = spacy.load('en_gutenberg')
        lut = dict([(x.col, x.symbol) for x in pd.read_sql_table('chunks_word_count_lut', dbengine).itertuples()])
        words = pd.read_sql_table('chunks_word_count', dbengine, index_col='index')
        testinds = [56, 45]
        testrows = [92, 1003, 242242]
        results = []
        for ind in testinds:
            testsymbol = lut[ind]
            for row in testrows:
                testcase = pickle.loads(words.loc[row].symbol_counts).toarray().flatten()
                results.append(testcase[ind] == np.array([testsymbol == str(w) for w in nlp(self.docs.loc[row].text)]).sum())
        self.assertTrue(all(results))

    def test_ngram1_count(self):
        lut = dict(pd.read_sql_table('chunks_1gram_count_lut', dbengine, index_col='col').itertuples())
        unigram = pd.read_sql_table('chunks_1gram_count', dbengine, index_col='index')
        testinds = [2, 86]
        testrows = [10, 2034, 103524]
        results = []
        for ind in testinds:
            testsymbol = lut[ind]
            for row in testrows:
                testcase = pickle.loads(unigram.loc[row].symbol_counts).toarray().flatten()
                results.append(testcase[ind] == len(re.findall(testsymbol, self.docs.loc[row].text.lower())))
        self.assertTrue(all(results))

    def test_ngram3_count(self):
        lut = dict(pd.read_sql_table('chunks_3gram_count_lut', dbengine, index_col='col').itertuples())
        trigram = pd.read_sql_table('chunks_3gram_count', dbengine, index_col='index')
        testinds = [35921, 16260]
        testrows = [10, 9381, 200310]
        results = []
        for ind in testinds:
            testsymbol = lut[ind]
            for row in testrows:
                testcase = pickle.loads(trigram.loc[row].symbol_counts).toarray().flatten()
                results.append(testcase[ind] == len(re.findall(testsymbol, self.docs.loc[row].text.lower())))
        self.assertTrue(all(results))

    def test_ngram5_count(self):
        lut = dict(pd.read_sql_table('chunks_5gram_count_lut', dbengine, index_col='col').itertuples())
        pentagram = pd.read_sql_table('chunks_5gram_count', dbengine, index_col='index')
        testinds = [43248, 80298]
        testrows = [52, 89358, 175293]
        results = []
        for ind in testinds:
            testsymbol = lut[ind]
            for row in testrows:
                testcase = pickle.loads(pentagram.loc[row].symbol_counts).toarray().flatten()
                results.append(testcase[ind] == len(re.findall(testsymbol, self.docs.loc[row].text.lower())))
        self.assertTrue(all(results))

class TestNgrams(unittest.TestCase):
    def test_reconcile(self):
        self.assertTrue(False)

if __name__ == '__main__':
    unittest.main()
