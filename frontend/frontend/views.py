from .backend import Detector, FakeDetector
from flask import render_template, request, jsonify, Markup
from frontend import app
from multiprocessing import Process
import json

app_title = 'Author Check'

detector = Detector('tcp://localhost:10000')
# detector = FakeDetector()

@app.errorhandler(404)
def pageNotFound(error):
    return app.send_static_file('404.html')

@app.route('/')
def index():
    return render_template("index.html",
                           title=app_title)

@app.route('/files', methods=['GET', 'POST'])
def files():
    mode = request.form['mode']
    raw = request.files['file_data'].read()
    try:
        text = raw.decode('utf-8')
        detector.enqueue(text, mode)
        error = ''
    except UnicodeDecodeError:
        error = 'UnicodeDecodeError thrown on file {}'.format(request.files['file_data'].filename)

    # TODO send the text to the process
    # TODO return results as ajax thingy
    return jsonify({'error': error})

@app.route('/result')
def result():
    result = detector.process_items()
    return render_template("result.html",
                           title = app_title,
                           metric = result['metric'],
                           prediction = result['prediction'],
                           fraction_greater = result['fraction_greater'],
                           same_class_hist = Markup(result['same_class_hist']),
                           samples = result['samples'],
                           query = result['query'])
